from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.views import View
from Sprint1 import commands
# Create your views here.
class Home(View):
    yourInstance = commands.Commands()
    def get(self,request):
        return render(request, 'main/index.html')
    def post(self, request, yourIn=yourInstance):
        commandInput = request.POST["command"]
        if commandInput:
            response = yourIn.command(commandInput)
        else:
            response = ""
        return render(request, 'main/index.html',{"message":response})