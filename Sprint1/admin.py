from django.contrib import admin
from Sprint1 import models

# Register your models here.
admin.site.register(models.User)
admin.site.register(models.Course)
admin.site.register(models.Section)